import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Ingredient } from './model/ingredient';

@Injectable()
export class IngredientService {
  constructor(private http: HttpClient) {}

  public fetchIngredients(): Observable<Ingredient[]> {
    return of([]);
  }
}
