import { Ingredient } from './ingredient';

export interface Meal {
  ingredients: Ingredient[];
}
