import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Meal } from './model/meal';

@Injectable()
export class MealService {
  constructor(private http: HttpClient) {}

  public addMeal(meal: Meal): Observable<Meal> {
    return of(meal);
  }
}
