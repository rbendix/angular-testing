import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { IngredientService } from './shared/ingredient.service';
import { ToastService } from './shared/toast.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: '',
        component: AppComponent,
        children: [{ path: 'meal-create', loadChildren: './unit/unit-test.module#UnitTestModule' }],
      },
    ]),
  ],
  providers: [ToastService, IngredientService],
  bootstrap: [AppComponent],
})
export class AppModule {}
