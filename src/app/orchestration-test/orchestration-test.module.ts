import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IngredientService } from '../shared/ingredient.service';
import { IngredientsListComponent } from './ingredients-list/ingredients-list.component';
import { OrchestrationTestComponent } from './orchestration-test.component';
import { OrchestrationTestStore } from './orchestration-test.store';

@NgModule({
  declarations: [
    OrchestrationTestComponent,
    IngredientsListComponent,
  ],
  providers: [
    OrchestrationTestStore,
    IngredientService,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
  ],
})
export class OrchestrationTestModule {}
