import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { OrchestrationTestService } from './orchestration-test.service';
import { Ingredient } from './orchestration.model';

@Injectable()
export class OrchestrationTestStore {

  private readonly ingredients: BehaviorSubject<Ingredient[]> = new BehaviorSubject([]);

  constructor(private readonly ingredientService: OrchestrationTestService) {
  }

  get ingredients$(): Observable<Ingredient[]> {
    return this.ingredients.asObservable();
  }

  fetchIngredients(): void {
    this.ingredientService.fetchIngredients()
    .pipe(take(1))
    .subscribe(ingredients => {
      this.ingredients.next(ingredients);
    });
  }

  addIngredient(ingredient: Ingredient): void {
    const currentIngredients: Ingredient[] = this.ingredients.getValue();
    const newIngredients: Ingredient[] = [...currentIngredients, ingredient];
    this.ingredients.next(newIngredients);
  }
}
