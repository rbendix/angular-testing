import { Component, Input } from '@angular/core';
import { Ingredient } from '../orchestration.model';

@Component({
  selector: 'app-ingredients-list',
  template: `
    <div [ngClass]="{'important': this.isImportantList}">
      <ul>
        <li *ngFor="let ingredient of this.ingredients">
          <span>{{ ingredient.name }} - {{ ingredient.calories }}</span>
        </li>
      </ul>
      <div class="secret-container"
           *ngIf="this.secretIngredient">
        <span>{{ this.secretIngredient.name }}</span>
      </div>
    </div>
  `,
  styleUrls: ['./ingredient-lists.component.css'],
})
export class IngredientsListComponent {
  @Input() ingredients: Ingredient[];
  @Input() secretIngredient: Ingredient;
  @Input() isImportantList: boolean;
}
