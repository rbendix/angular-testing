import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Ingredient } from '../orchestration.model';
import { IngredientsListComponent } from './ingredients-list.component';

describe('IngredientsListComponent', () => {

  let fixture: ComponentFixture<IngredientsListComponent>;
  let component: IngredientsListComponent;

  beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [IngredientsListComponent],
      }).compileComponents();
    },
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientsListComponent);
    component = fixture.componentInstance;
  });

  it('should render a list of ingredients', () => {
    const expectedIngredients: Ingredient[] = [
      { name: 'Spaghetti', calories: 500 },
      { name: 'Cheese', calories: 234 },
      { name: 'Eggs', calories: 231 },
    ];

    component.ingredients = expectedIngredients;
    fixture.detectChanges();

    const template: DebugElement = fixture.debugElement;
    const listElements: DebugElement[] = template.queryAll(By.css('li'));

    expect(listElements.length).toEqual(expectedIngredients.length);
  });

  it('should show a secret ingredient', () => {
    const expectedIngredient: Ingredient = { name: 'Mushrooms', calories: 10 };

    component.secretIngredient = expectedIngredient;
    fixture.detectChanges();

    const template: DebugElement = fixture.debugElement;
    const secretContainer: DebugElement = template.query(By.css('.secret-container'));

    expect(secretContainer).toBeTruthy();
    expect(secretContainer.nativeElement.textContent).toEqual(expectedIngredient.name);
  });

  it('should not show a secret ingredient', () => {
    fixture.detectChanges();

    const template: DebugElement = fixture.debugElement;
    const secretContainer: DebugElement = template.query(By.css('.secret-container'));

    expect(secretContainer).toBeFalsy();
  });

  it('should apply the important style class to the list container', () => {
    component.isImportantList = true;
    fixture.detectChanges();

    const template: DebugElement = fixture.debugElement.query(By.css('.important'));

    expect(template).toBeTruthy();
  });

  it('should not apply the important style class to the list container', () => {
    fixture.detectChanges();

    const template: DebugElement = fixture.debugElement.query(By.css('.important'));

    expect(template).toBeFalsy();
  });
});
