export interface Ingredient {
  readonly name: string;
  readonly calories: number;
}
