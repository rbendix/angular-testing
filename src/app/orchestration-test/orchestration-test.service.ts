import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ingredient } from './orchestration.model';

@Injectable()
export class OrchestrationTestService {

  constructor(private readonly http: HttpClient) {
  }

  fetchIngredients(): Observable<Ingredient[]> {
    return this.http.get<Ingredient[]>('http://localhost:4201/ingredients');
  }
}
