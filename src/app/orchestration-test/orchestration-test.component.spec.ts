import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Route, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { OrchestrationTestComponent } from './orchestration-test.component';
import { OrchestrationTestService } from './orchestration-test.service';
import { OrchestrationTestStore } from './orchestration-test.store';
import { Ingredient } from './orchestration.model';
import Spy = jasmine.Spy;

describe('OrchestrationTestComponent', () => {

  const routes: Route[] = [
    { path: '', component: OrchestrationTestComponent },
  ];

  let fixture: ComponentFixture<OrchestrationTestComponent>;
  let component: OrchestrationTestComponent;
  let http: HttpTestingController;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrchestrationTestComponent],
      providers: [OrchestrationTestStore, OrchestrationTestService],
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes)],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchestrationTestComponent);
    component = fixture.componentInstance;
    http = TestBed.get(HttpTestingController);
    router = TestBed.get(Router);
  });

  it('should fetch all ingredients', () => {
    const expectedIngredients: Ingredient[] = [
      { name: 'Spaghetti', calories: 300 },
      { name: 'Cheese', calories: 200 },
    ];

    fixture.detectChanges();

    const testRequest: TestRequest = http.expectOne('http://localhost:4201/ingredients');
    expect(testRequest.request.method).toEqual('GET');
    testRequest.flush(expectedIngredients);

    component.ingredients$.subscribe(ingredients => {
      expect(ingredients).toEqual(expectedIngredients);
    });
  });

  it('should add a random ingredient', () => {
    const initialIngredients: Ingredient[] = [
      { name: 'Spaghetti', calories: 300 },
      { name: 'Cheese', calories: 200 },
    ];

    fixture.detectChanges();

    const testRequest: TestRequest = http.expectOne('http://localhost:4201/ingredients');
    testRequest.flush(initialIngredients);

    component.addRandomIngredient();

    component.ingredients$.subscribe(ingredients => {
      expect(ingredients.length).toEqual(initialIngredients.length + 1);
    });
  });

  it('should navigate to home on back button press', () => {
    fixture.detectChanges();

    const routerSpy: Spy = spyOn(router, 'navigate');

    component.navigateBack();

    expect(routerSpy).toHaveBeenCalledWith(['']);
  });
});
