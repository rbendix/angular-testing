import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { OrchestrationTestStore } from './orchestration-test.store';
import { Ingredient } from './orchestration.model';

@Component({
  selector: 'app-orchestration-test-component',
  templateUrl: './orchestration-test.component.html',
  styleUrls: ['./orchestration-test.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrchestrationTestComponent implements OnInit, OnDestroy {

  ingredients$: Observable<Ingredient[]>;

  private readonly onDestroyTriggered$: Subject<void> = new Subject();

  constructor(private readonly store: OrchestrationTestStore,
              private readonly router: Router) {
  }

  ngOnInit(): void {
    this.ingredients$ = this.store.ingredients$.pipe(
      takeUntil(this.onDestroyTriggered$),
    );
    this.store.fetchIngredients();
  }

  ngOnDestroy(): void {
    this.onDestroyTriggered$.next();
    this.onDestroyTriggered$.complete();
  }

  addRandomIngredient(): void {
    this.store.addIngredient({ name: 'Actual random', calories: 300 });
  }

  navigateBack(): void {
    this.router.navigate(['']);
  }
}
