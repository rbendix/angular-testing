import { Ingredient } from '../shared/model/ingredient';

export function ingredientIncompatible(ingredient: Ingredient, ingredients: Ingredient[]): boolean {
  return ingredients.filter(ig => ig.name === 'Spaghetti').length > 0 && ingredient.name === 'Ketchup' ||
      ingredients.filter(ig => ig.name === 'Ketchup').length > 0 && ingredient.name === 'Spaghetti';
}
