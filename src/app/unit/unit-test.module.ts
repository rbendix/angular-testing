import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MealCompositorComponent } from './meal-compositor.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  providers: [],
  entryComponents: [],
  declarations: [MealCompositorComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {path: '', component: MealCompositorComponent}
    ])
  ],
})
export class UnitTestModule {
}
