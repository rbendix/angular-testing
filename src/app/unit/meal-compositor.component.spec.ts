import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MealCompositorComponent } from './meal-compositor.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToastService } from '../shared/toast.service';
import { IngredientService } from '../shared/ingredient.service';
import { FormsModule } from '@angular/forms';

describe('MealCompositorComponent', () => {
  let component: MealCompositorComponent;
  let fixture: ComponentFixture<MealCompositorComponent>;
  let toastServiceSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ToastService, IngredientService],
      imports: [HttpClientTestingModule, FormsModule],
      declarations: [ MealCompositorComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealCompositorComponent);
    component = fixture.componentInstance;

    toastServiceSpy = spyOn(TestBed.get(ToastService), 'showToast').and.callFake(() => {});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not allow Spaghetti with Ketchup', () => {
    component.selectedIngredient = {name: 'Ketchup', calories: 120};
    component.add();
    expect(toastServiceSpy).toHaveBeenCalledTimes(1);
  });
});
