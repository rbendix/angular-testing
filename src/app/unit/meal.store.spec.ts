import { MealStore } from './meal.store';
import { MealService } from '../shared/meal.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;
import createSpy = jasmine.createSpy;
import { of } from 'rxjs';

describe('MealStore', () => {
  const observer = createSpy('Meals spy');

  let mealServiceSpy: SpyObj<MealService>;
  let mealStore: MealStore;

  beforeEach(() => {
    mealServiceSpy = createSpyObj<MealService>('MealService', ['addMeal']);

    mealStore = new MealStore(mealServiceSpy);
    mealStore.getMeals().subscribe(observer);
  });

  it('should add meal to the state', () => {
    const meal = {ingredients: [{name: 'Ham', calories: 100}]};
    mealServiceSpy.addMeal.and.callFake(() => of(meal));

    mealStore.addMeal(meal);

    expect(observer).toHaveBeenCalledWith([meal]);
  });
});
