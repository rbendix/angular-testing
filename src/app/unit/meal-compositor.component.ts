import { Component } from '@angular/core';
import { IngredientService } from '../shared/ingredient.service';
import { Ingredient } from '../shared/model/ingredient';
import { ToastService } from '../shared/toast.service';
import { ingredientIncompatible } from './meal.compositor';

@Component({
  selector: 'cl-meal-compositor',
  template: `
    <h1>Meal</h1>
    <select id="ingredient-selection" [(ngModel)]="selectedIngredient">
      <option *ngFor="let i of availableIngredients" [ngValue]="i">{{i.name}}</option>
    </select>
    <button id="add-ingredient" (click)="add()">Add</button>
    <p class="ingredient" *ngFor="let ingredient of ingredients">{{ingredient.name}}</p>
  `,
  styles: []
})
export class MealCompositorComponent {

  selectedIngredient: Ingredient;

  availableIngredients: Ingredient[] = [
    {name: 'Mushrooms', calories: 50},
    {name: 'Ketchup', calories: 175}
  ];

  ingredients: Ingredient[] = [
    {name: 'Spaghetti', calories: 250},
    {name: 'Cheese', calories: 250},
  ];

  constructor(private ingredientService: IngredientService,
              private toastService: ToastService) {}

  public add(): void {

    if (ingredientIncompatible(this.selectedIngredient, this.ingredients)) {
      this.toastService.showToast('Pasta and Ketchup? Are you mad?');
    } else {
      this.ingredients.push(this.selectedIngredient);
    }
  }

}
