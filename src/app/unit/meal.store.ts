import { Injectable } from '@angular/core';
import { MealService } from '../shared/meal.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Meal } from '../shared/model/meal';

@Injectable()
export class MealStore {

  private meals: BehaviorSubject<Meal[]> = new BehaviorSubject<Meal[]>([]);

  constructor(private mealService: MealService) {}

  public getMeals(): Observable<Meal[]> {
    return this.meals.asObservable();
  }

  public addMeal(meal: Meal): void {
    this.mealService.addMeal(meal).subscribe(m => {

      const meals = this.meals.value.splice(0);
      meals.push(m);

      this.meals.next(meals);
    });
  }
}
