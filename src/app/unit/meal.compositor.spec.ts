import { Ingredient } from '../shared/model/ingredient';
import { ingredientIncompatible } from './meal.compositor';

describe('MealCompositionFunctions', () => {

  it('should detect spaghetti and ketchup as incompatible', () => {
    const ingredientsWithSpaghetti: Ingredient[] = [{name: 'Spaghetti', calories: 300}];
    const ketchup: Ingredient = {name: 'Ketchup', calories: 150};

    const ingredientsIncompatible = ingredientIncompatible(ketchup, ingredientsWithSpaghetti);

    expect(ingredientsIncompatible).toBe(true);
  });

  it('should detect ketchup and spaghetti as incompatible', () => {
    const ingredients: Ingredient[] = [{name: 'Ketchup', calories: 150}];
    const ketchup: Ingredient = {name: 'Spaghetti', calories: 300};

    const ingredientsIncompatible = ingredientIncompatible(ketchup, ingredients);

    expect(ingredientsIncompatible).toBe(true);
  });

  it('should detect ham and cheese as compatible', () => {
    const ingredients: Ingredient[] = [{name: 'Ham', calories: 300}];
    const cheese: Ingredient = {name: 'Cheese', calories: 150};

    const ingredientsIncompatible = ingredientIncompatible(cheese, ingredients);

    expect(ingredientsIncompatible).toBe(false);
  });
});
