import { browser, by, element, ElementArrayFinder, protractor } from 'protractor';

const EC = protractor.ExpectedConditions;

export class MealCompositorPage {
  navigateTo() {
    return browser.get('meal-create') as Promise<any>;
  }

  addIngredient(): void {
    const btn = element(by.id('add-ingredient'));
    browser.wait(EC.visibilityOf(btn), 10000);
    btn.click();
  }

  getIngredients(): ElementArrayFinder {
    return element.all(by.css('.ingredient'));
  }

  selectIngredient(index: number): void {
    const select = element(by.id('ingredient-selection'));
    browser.wait(EC.visibilityOf(select), 10000);
    select.all(by.css('option')).then(options => {
      options[index].click();
    });

  }
}
