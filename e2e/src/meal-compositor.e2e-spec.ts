import { MealCompositorPage } from './meal-compositor.po';
import { ElementArrayFinder } from 'protractor';

describe('MealCompositor', () => {
  let page: MealCompositorPage;

  beforeEach(() => {
    page = new MealCompositorPage();
    page.navigateTo();
  });

  it('should have additional ingredient after click', () => {
    page.getIngredients().count().then(oldLength => {

      page.selectIngredient(0);
      page.addIngredient();

      page.getIngredients().then(ingredients => expect(ingredients.length).toBe(oldLength + 1));
    });
  });

  it('should not add ketchup if ingredients contain spaghetti', () => {
    page.getIngredients().count().then(oldLength => {

      page.selectIngredient(1);
      page.addIngredient();

      page.getIngredients().then((ingredients: ElementArrayFinder[]) => {
        expect(ingredients.length).toBe(oldLength);
        Promise.all(ingredients.map((value: ElementArrayFinder) =>
            value.getText())).then(texts => expect(texts).not.toContain('Ketchup'));
      });
    });
  });
});
